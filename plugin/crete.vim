" Autocreate parent directories when a buffer is written.
" Author: Evan Hanson <evhan@foldling.org>
" Version: 0.0.1
" Homepage: http://git.foldling.org/vim-crete
" Repository: http://git.foldling.org/vim-crete.git
" Last Change: 2016-07-26
" License: BSD

if exists('g:crete_loaded') || &compatible || version < 700
  finish
else
  let g:crete_throw = 0
  let g:crete_loaded = 1
endif

function! CreteWrite()
  let l:dir = expand('%:p:h')
  try
    execute system('/bin/mkdir -p ' . fnamemodify(l:dir, ':S'))
  catch /.*/
    let l:msg = 'Could not create parent directory: ' . l:dir
    echohl WarningMsg
    echomsg l:msg
    echohl None
    if g:crete_throw == 1 | echoerr l:msg | endif
  endtry
endfunction

augroup crete
  autocmd!
  autocmd BufWritePre * nested call CreteWrite()
augroup END
