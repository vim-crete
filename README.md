vim-crete
=========
Autocreate parent directories when a buffer is written.

Usage
-----
This plugin automatically creates all missing parent directories when a
buffer is written to a nonexistent path.

For example, if directory "foo" exists and the user edits
"foo/bar/baz.txt", the `:write` command will silently create "foo/bar"
before the buffer is written.

History
-------
 * 0.0.1 - Initial release.

Licence
------
Copyright © 2016 Evan Hanson <evhan@foldling.org>

BSD-style license. See `LICENSE` for details.
